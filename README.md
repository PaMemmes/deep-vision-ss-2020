# Deep Vision project
This repository holds content for the final project of the course Deep Vision in the summer semester 2020.
In order for the code to work, a cuda GPU must be used and it has to be recognized by pytorch. Otherwise the code will only work with some minor modifications. The dataset used was: https://www.kaggle.com/paultimothymooney/chest-xray-pneumonia. It needs to be at the same level as network.ipynb and dataexploration.ipynb.
## Subject
Use of transfer learning in order to get good accuracy on the x-ray dataset about pneunomia. 